package com.simple.idea.plugin.know.config;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 项目: idea-plugins
 *
 * 功能描述: 线程配置
 *
 * @author: WuChengXing
 *
 * @create: 2023-07-27 09:31
 **/
public class PluginThreadPool {

    public static final Integer CORE_THREAD_NUM = 5;

    public static final Integer MAX_THREAD_NUM = 10;

    /**
     * 空闲线程回收时间
     */
    public static final Integer THREAD_CYCLE_TIME = 60;


    private static ThreadPoolExecutor pluginThreadPool() {
        return new ThreadPoolExecutor(CORE_THREAD_NUM, MAX_THREAD_NUM, THREAD_CYCLE_TIME, TimeUnit.SECONDS, new ArrayBlockingQueue<>(10));
    }

    public static ThreadPoolExecutor PLUGIN_THREAD_POOL = PluginThreadPool.pluginThreadPool();
}
